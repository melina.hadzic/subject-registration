-- Frontend app build:

cd client-app

npm install

npm run start

-- In case of importing issues:

npm i @mui/material react-router-dom@5 @mui/icons-material @emotion/react @emotion/styled @mui/styled-engine

-- Backend app build: 

cd API

dotnet watch --no-hot-reload