import './App.css';
import Navbar from './common/navbar/Navbar';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Routes } from './common/routes';
import { SearchClasses } from './common/search-classes/SearchClasses';
import { RequestsList } from './views/components/requests-list/RequestsList';
import UserContextProvider from "./common/contexts/user-context/UserContextProvider";
import Homepage from './views/components/homepage/Homepage';
import { SubjectsList } from './views/components/subjects-list/SubjectsList';
import { RegisteredSubjects } from './views/components/registered-subjects/RegisteredSubjects';
import ListValuesContextProvider from './common/contexts/list-values-context/ListValuesContextProvider';
import {NewUser} from "./views/components/new-user/NewUser";
import {AdminPage} from "./views/components/admin-page/AdminPage";

function App() {
  return (
    <UserContextProvider>
      <ListValuesContextProvider>
        <div>
          <Navbar />
          <Switch>
            <Route path={Routes.adminPage.path} component={AdminPage} />
            <Route path={Routes.newUser.path} component={NewUser} />
            <Route path={Routes.registeredSubjects.path} component={RegisteredSubjects} />
            <Route path={Routes.requests.path} component={RequestsList} />
            <Route path={Routes.registerSubjects.path} component={SearchClasses} />
            <Route path={Routes.subjects.path} component={SubjectsList} />
            <Route path={Routes.homepage.path} component={Homepage} />
          </Switch>
        </div>
      </ListValuesContextProvider>
    </UserContextProvider>
  );
}

export default App;
