import { Dimmer, Loader } from "semantic-ui-react";

export default function LoadingComponent(props: {
    inverted: true,
    content: 'Loading...'
}) {
    return <Dimmer active={true} inverted={props.inverted}>
        <Loader content={props.content} />
    </Dimmer>
}