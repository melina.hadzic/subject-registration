import React, {useEffect, useState} from "react";
import {User} from "../../../common/api/dto-objects";
import agent from "../../../common/api/agent";
import {Button, Typography} from "@material-ui/core";
import {classes} from "../../../Common.styles";
import {Role} from "../../../common/contexts/user-context/UserContext";
import {Grid} from "@mui/material";

export const AdminPage = (): JSX.Element => {
    const [usersToApprove, setUsersToApprove] = useState<User[]>([])

    const getUsers = (): void => {
        void (async () => {
            const users = await agent.Users.list()
            setUsersToApprove(users.filter(user => !user.isApproved))
        })()
    }

    useEffect(() => {
        getUsers()
    }, [])

    const approveRequest = (user: User): void => {
        void(async () => {
            if(user) {
                await agent.Users.update(user)
                getUsers()
            }
        })()
    }

    const rejectRequest = (auth0id: string): void => {
        void(async () => {
            await agent.Users.delete(auth0id)
            getUsers()
        })()
    }

    return <div style={{ margin: "50px"}}>
        <h1>Verifikacija novih registrovanih korisnika u skladu sa njihovim funkcijama</h1>
        {usersToApprove.map(user => (
            <Grid item xs={6} key={user.auth0id} style={{ ...classes.requestCard, padding: "10px", display: "block" }}>
                <Typography style={{ ...classes.subjectTitle }} >Zahtjev za ulogu: {user.role===Role.Teacher ? "PROFESOR" : "STUDENT"}</Typography>
                <Typography style={{ fontSize: "16px", margin: "10px"}} >Ime: {user.name}</Typography>
                <Typography style={{ fontSize: "16px", margin: "10px" }} >Email: {user.email}</Typography>
                {user.role === Role.Student && <Typography style={{ fontSize: "16px", margin: "10px" }} >Broj indeksa: {user.studentIndex}</Typography>}
                <Button style={{ border: "1px solid rgb(247, 122, 38)", margin: "5px" }} onClick={() => approveRequest(user)}>ODOBRI ZAHTJEV</Button>
                <Button style={{ border: "1px solid rgb(247, 122, 38)", margin: "5px" }} onClick={() => rejectRequest(user.auth0id)}>ODBIJ ZAHTJEV</Button>
            </Grid>
            ))
                }
    </div>
}
