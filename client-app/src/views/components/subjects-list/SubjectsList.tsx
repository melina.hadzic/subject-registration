import { Grid, Typography } from "@material-ui/core";
import { useEffect, useState } from "react"
import agent from "../../../common/api/agent";
import { Subject } from "../../../common/api/dto-objects"
import { Role, useUserContext } from "../../../common/contexts/user-context/UserContext";
import LoadingComponent from "../../../LoadingComponent";
import * as React from "react";
import {classes} from "../../../common/logout-button/LogoutButton.styles";
import {subjectsListStyles} from "./SubjectsList.styles";
import {SubjectCard} from "../../../common/subject-card/SubjectCard";

export const SubjectsList: React.FC = () => {
    const [subjects, setSubjects] = useState<Subject[]>();
    const { userDto } = useUserContext()
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        agent.Subjects.list().then(allSubjects => {
            if (userDto.role === Role.Teacher) {
                const filteredSubjects = allSubjects.filter(subject => subject.teacherId === userDto.id)
                setSubjects(filteredSubjects)
            }
            else if (userDto.role === Role.Student) {
                void (async () => {
                    const allPassedSubjects = await agent.PassedSubjects.list(userDto.id)
                    const mappedSubjects = allSubjects.filter(subject => allPassedSubjects.find(item => item.code === subject.code))
                    setSubjects(mappedSubjects)
                })()
            }
            setLoading(false)
        })
    }, [userDto])

    if (loading || subjects === undefined) return <LoadingComponent inverted={true} content={'Loading...'} />
    if (subjects?.length === 0) return <Typography style={{ ...classes.mainTitle }}>Nema dostupnih položenih predmeta za ovog studenta.</Typography>

    return (
        subjects?.length === 0 ? <Typography style={{ ...classes.mainTitle }}>Nema registrovanih predmeta.</Typography> :
            <div style={{ ...subjectsListStyles.title}}>
                <Typography style={{ ...classes.mainTitle }}>PREGLED PREDMETA</Typography>
                <Grid container>
                    {
                        subjects?.map(registeredSubject => (
                            <Grid item xs={5} style={{ width: "700px"}}>
                                <SubjectCard registeredSubject={registeredSubject} />
                            </Grid>
                        ))
                    }
                </Grid>
            </div>
    )
}
