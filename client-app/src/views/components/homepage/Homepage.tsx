import HomepageCard from "../../../common/homepage-card/HomepageCard"
import {Role, useUserContext} from "../../../common/contexts/user-context/UserContext";
import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { Box, CircularProgress } from "@mui/material";

const Homepage: React.FC = () => {
    const { userDto } = useUserContext();
    const { isLoading } = useAuth0();

    return (
        <div style={{ marginTop: "60px" }}>
            { isLoading ? <Box sx={{ marginLeft: "1000px", marginTOp: "900px"}} display="flex" alignContent="center"><CircularProgress /></Box> :
            <div style={{ display: "flex", flexDirection: "row", marginLeft: "30px" }}>
                <HomepageCard img={0} title="Pregled predmeta" description="Obavi pregled predmeta za trenutnu akademsku godinu." buttonTitle="SAZNAJ VIŠE" path="/subjects/passed" />
                {userDto.role !== Role.Teacher && <HomepageCard img={2} title="Registracija predmeta"
                    description="Obavi registraciju predmeta za trenutnu akademsku godinu."
                    buttonTitle="SAZNAJ VIŠE" path="/subjects/tegister" />
                }
                {userDto.role !== Role.Teacher && <HomepageCard img={1} title="Registrovani predmeti"
                    description="Obavi pregled registrovanih predmeta za trenutnu akademsku godinu."
                    buttonTitle="SAZNAJ VIŠE" path="/subjects/registered" />
                }
                <HomepageCard img={3} title="Trenutni zahtjevi" description="Pregledaj trenutne zahtjeve za registraciju predmeta vezane za tvoj nalog." buttonTitle="SAZNAJ VIŠE" path="/subjects/requests" />
            </div>
            }   
        </div>
    )
}

export default Homepage
