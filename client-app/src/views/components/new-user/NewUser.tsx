import {Role} from "../../../common/contexts/user-context/UserContext";
import {useState} from "react";
import {useAuth0} from "@auth0/auth0-react";
import * as Mui from "@material-ui/core";
import * as React from "react";
import Stack from "@mui/material/Stack";
import {classes} from "../../../common/search-classes/SearchClasses.styles";
import {Button} from "semantic-ui-react";
import agent from "../../../common/api/agent";
import { User } from "../../../common/api/dto-objects";

export const NewUser = (): JSX.Element => {
    const {user} = useAuth0()

    const [newUser, setNewUser] = useState<User>({
        auth0id: user?.sub?.split("|")[1] ?? "Auth0-error",
        name: "",
        role: Role.Guest as number,
        studentIndex: "",
        email: user?.name ?? "Auth0-error",
        isApproved: false,
    })

    const [studentIndex, setStudentIndex] = useState("")

    const sendRequestForUserRegistration = () => {
        if (newUser.role === Role.Student) {
            console.log(newUser)
            console.log(user)
            setNewUser({...newUser, studentIndex: studentIndex})
        }
        void(async () => {
            console.log(newUser)
            await agent.Users.create(newUser)
            const a = await agent.Users.list()
            console.log(a)
        })()

    }

    const options = [
        {
            key: Role.Student,
            label: "Student"
        }, {
            key: Role.Teacher,
            label: "Profesor"
        }]

    return <Stack direction="column">
        <div style={{margin: "30px", display: "inline-block", width: "450px", fontSize: "large"}}>
            <label>Ime i prezime: </label>
            <input onChange={(value) => setNewUser({...newUser, name: value.target.value})}/>
        </div>
        <div style={{margin: "30px", display: "inline-block", width: "450px", fontSize: "large"}}>
            <label>Registruj se kao: </label>
            <Mui.Select
                style={{ width: "100px", padding: "5px"}}
                placeholder="Izaberi ulogu"
                value={newUser.role}
                onChange={(value, b) => setNewUser({...newUser, role: value.target.value as number})}
            >
                {options.map((option) => (
                    <Mui.MenuItem value={option.key}>
                        {option.label}
                    </Mui.MenuItem>
                ))}
            </Mui.Select>
        </div>
        {newUser.role === Role.Student && <>
            <div style={{margin: "30px", display: "inline-block", width: "450px", fontSize: "large"}}>
                <label>Unesite broj indeksa: </label>
                <input onChange={value => setStudentIndex(value.target.value)}/>
            </div>
        </>}
        {
            newUser.role !== Role.Guest &&
            <Button
                disabled={newUser.role === Role.Student && studentIndex === ""}
                onClick={sendRequestForUserRegistration}
                style={{ ...classes.registrationButton, marginLeft: "20px" }}
            >
                Pošalji zahtjev za registraciju korisnika
            </Button>
        }
    </Stack>
}
