import { Button, Typography } from "@material-ui/core"
import React, { useEffect, useState } from "react"
import agent from "../../../common/api/agent"
import uuid from "react-uuid"
import { Request } from "../../../common/api/dto-objects"
import { Role, useUserContext } from "../../../common/contexts/user-context/UserContext"
import { useListValuesContext } from "../../../common/contexts/list-values-context/ListValuesContext"
import { classes } from "../../../Common.styles";
import { Grid } from "semantic-ui-react"
import LoadingComponent from "../../../LoadingComponent";
import {requestsListStyles} from "./RequestsList.styles";

export const RequestsList: React.FC = () => {
    const { teachers, requests, students, subjects } = useListValuesContext()
    const [currentRequests, setCurrentRequests] = useState<Request[]>(requests)
    const [loading, setLoading] = useState(true)
    const { userDto } = useUserContext()

    const loadRequests = () => {
        void (async () => {
            if (teachers) {
                const currentTeacher = teachers.find(teacher => teacher.auth0Id === userDto.id)
                if (currentTeacher) {
                    agent.Requests.list().then(value => {
                        setCurrentRequests(value.filter(request => request.teacherId === currentTeacher.auth0Id))
                    })
                } else {
                    const currentStudent = students?.find(student => student.auth0Id === userDto.id)
                    if (currentStudent) {
                        agent.Requests.list().then(value => {
                            setCurrentRequests(value.filter(request => request.studentId === currentStudent.auth0Id))
                        })
                    }
                }
                setLoading(false)
            }
        })()
    }

    useEffect(() => {
        setLoading(true)
        loadRequests()
        setLoading(false)
    }, [userDto.id, teachers, students])

    const approveRequest = (id: string) => {
        const request = currentRequests.find(item => item.id === id)
        const index = currentRequests.findIndex(item => item.id === id)
        if (request) {
            void (async () => {
                const temp = { ...request }
                await agent.Requests.delete(request.id)
                temp.status = 1 // approved
                temp.id = uuid()
                await agent.Requests.create(temp)
            })()
            const copyArray = [...currentRequests]
            copyArray[index].status = 1
            setCurrentRequests(copyArray)
        }

    }

    const rejectRequest = (id: string) => {
        const request = currentRequests.find(item => item.id === id)
        const index = currentRequests.findIndex(item => item.id === id)
        if (request) {
            void (async () => {
                const temp = { ...request }
                await agent.Requests.delete(request.id)
                temp.status = 2 // rejected
                temp.id = uuid()
                await agent.Requests.create(temp)
            })()
            const copyArray = [...currentRequests]
            copyArray[index].status = 2
            setCurrentRequests(copyArray)
        }
    }

    function getPrerequisiteList(request: Request): JSX.Element {
        const codes = request.missingPrerequisites.split(",")
        const list = codes.map(code => subjects?.find(subject => subject.code === code)?.name ?? "Oopsie")
        return <>
            {
                list.map((prerequisite: string) => <li key={prerequisite}> {prerequisite} </li>)
            }
        </>
    }

    if (loading) return <LoadingComponent inverted={true} content={'Loading...'} />

    return (
        <div style={{ ...requestsListStyles.container }}>
            {currentRequests.length === 0 ? <Typography style={{ ...classes.mainTitle }}>Nemate poslanih zahtjeva.</Typography> : <Typography style={{ ...classes.mainTitle }}>LISTA ZAHTJEVA</Typography>}
            <Grid>
                {currentRequests.map(request => (
                    <Grid item xs={6} key={request.id} style={{ ...classes.requestCard, width: "670px", padding: "10px", display: "block" }}>
                        <Typography style={{ ...classes.subjectTitle }} ><i>Predmet koji student želi prijaviti:</i> 
                        <br></br>
                        {subjects?.find(subject => subject.code === request.subjectCode)?.name}</Typography>
                        <Typography style={{ fontSize: "16px", margin: "10px" }}>Preduslovi predmeta koje student ne ispunjava: {getPrerequisiteList(request)}</Typography>
                        <Typography style={{ ...requestsListStyles.fontStyleLarger }}>Student: {students?.find(student => student.auth0Id === request.studentId)?.name}</Typography>
                        <Typography style={{ ...requestsListStyles.fontStyle }}>Profesor: {teachers?.find(teacher => teacher.auth0Id === request.teacherId)?.name}</Typography>
                        <Typography style={{ ...requestsListStyles.fontStyle }}>Status: <b>{request.status === 1 ? "ODOBRENO" : request.status === 2 ? "ODBIJENO" : "NA ČEKANJU"}</b></Typography>
                        <Button style={userDto.role === Role.Student ? { display: "none" } : { border: "1px solid rgb(247, 122, 38)", margin: "5px" }} disabled={request.status === 1} onClick={() => approveRequest(request.id)}>ODOBRI ZAHTJEV</Button>
                        <Button style={userDto.role === Role.Student ? { display: "none" } : { border: "1px solid rgb(247, 122, 38)", margin: "5px" }} disabled={request.status === 2} onClick={() => rejectRequest(request.id)}>ODBIJ ZAHTJEV</Button>

                    </Grid>
                ))}
            </Grid>
        </div>
    )
}
