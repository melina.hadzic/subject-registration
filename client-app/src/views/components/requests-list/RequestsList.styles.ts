export const requestsListStyles = {
    container: {
        margin: "20px",
        marginTop: "60px ",
        padding: "10px"
    },
    fontStyle: {
        fontSize: "14px",
        margin: "10px"
    },
    fontStyleLarger: {
        fontSize: "16px",
        margin: "10px"
    },
}
