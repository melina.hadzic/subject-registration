import { Typography } from "@material-ui/core";
import Stack from '@mui/material/Stack';
import * as React from "react";
import { useEffect, useState } from "react";
import { classes } from "../../../Common.styles";
import { RegisteredSubject } from "../../../common/api/dto-objects";
import { useListValuesContext } from "../../../common/contexts/list-values-context/ListValuesContext";
import { registeredSubjectsStyle } from "./RegisteredSubjects.styles";
import LoadingComponent from "../../../LoadingComponent";
import agent from "../../../common/api/agent";
import { useUserContext } from "../../../common/contexts/user-context/UserContext";
import {SubjectCard} from "../../../common/subject-card/SubjectCard";

export const RegisteredSubjects: React.FC = () => {
    const { subjects } = useListValuesContext()
    const [registeredSummerSubjects, setRegisteredSummerSubjects] = useState<RegisteredSubject[]>([])
    const [registeredWinterSubjects, setRegisteredWinterSubjects] = useState<RegisteredSubject[]>([])
    const [loading, setLoading] = useState(true)
    const { userDto } = useUserContext()

    useEffect(() => {
        void (async () => {
            const registeredSubjects = await agent.RegisteredSubjects.list(userDto.id)
            if (registeredSubjects) {
                registeredSubjects.forEach(item => {
                    const subject = subjects.find(s => s.code === item.code)
                    if (subject) {
                        if (subject.isWinterSemesterSubject) {
                            setRegisteredWinterSubjects(prev => [...prev, item])
                        } else {
                            setRegisteredSummerSubjects(prev => [...prev, item])
                        }
                    }
                })
            }
        })();

        setTimeout(() => {
            setLoading(false)
        }, 1000)
    }, [subjects])

    if (loading) return <LoadingComponent inverted={true} content={'Loading...'} />

    return (
        <div style={{ ...registeredSubjectsStyle.container }}>
            { registeredWinterSubjects.length === 0 && registeredSummerSubjects.length === 0 ? 
            <Typography style={{ ...classes.mainTitle}}>Nema registrovanih predmeta.</Typography> :
                <>
                <Typography style={{ ...classes.mainTitle, marginLeft: "490px", }}>REGISTROVANI PREDMETI</Typography>
                <Stack direction="row">
                    <div style={{ width: "600px" }}>
                        <Typography style={{ ...registeredSubjectsStyle.heading }}>ZIMSKI PREDMETI</Typography>
                        {
                            registeredWinterSubjects?.map(registeredSubject => (
                                <SubjectCard registeredSubject={registeredSubject} />
                            ))
                        }
                    </div>
                    <div style={{ ...registeredSubjectsStyle.rightContainer }}>
                        <Typography style={{ ...registeredSubjectsStyle.heading }}>LJETNI PREDMETI</Typography>
                        {
                            registeredSummerSubjects?.map(registeredSubject => (
                                <SubjectCard registeredSubject={registeredSubject} />
                            ))
                        }
                    </div>
                </Stack>
                </>
}
            </div>
    )
}
