export const registeredSubjectsStyle = {
  subjectTitle: {
    fontSize: "18px",
    fontWeight: "600",
    padding: "10px",
    width: "550px",
    color: "white",
    margin: "10px",
    backgroundColor: "rgb(287, 162, 78)",
  },
  heading: {
    marginLeft: "150px",
    fontSize: "18px",
    letterSpacing: "2px",
  },
  container: {
    marginTop: "60px",
    marginLeft: "10px"
  },
  rightContainer: {
    width: "600px",
    marginLeft: "80px"
  }
};
