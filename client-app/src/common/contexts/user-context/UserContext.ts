import React, { useContext } from "react";

export enum Role {
  Student,
  Teacher,
  Guest,
}

export interface UserDto {
  id: string;
  role: Role;
  name: string;
}

export interface UserContextValue {
  userDto: UserDto;
  setUserDto: (updatedUser: UserDto) => void;
}

export const guestUserDto = {
  id: "",
  role: Role.Guest,
  name: "Guest",
};

export const studentUserDto = {
  id: "111446754165250466163",
  role: Role.Student,
  name: "Melina Hadzic",
};

export const teacherUserDto = {
  id: "63d813c37c9379f5545d0613",
  role: Role.Teacher,
  name: "Teacher",
};

export const defaultValue: UserContextValue = {
  userDto: guestUserDto,
  setUserDto: function (updatedUser: UserDto): void {},
};

export const UserContext = React.createContext<UserContextValue>(defaultValue);
export const useUserContext = (): UserContextValue => useContext(UserContext);
