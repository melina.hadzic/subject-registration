import { useAuth0 } from "@auth0/auth0-react";
import { PropsWithChildren, useEffect, useState } from "react"
import { guestUserDto, studentUserDto, teacherUserDto, UserContext, UserDto } from "./UserContext";
import {useHistory} from "react-router-dom";

function UserContextProvider(props: PropsWithChildren) {
    const [userDto, setUserDto] = useState<UserDto>(guestUserDto);
    const { user } = useAuth0();
    const history = useHistory();

    useEffect(() => {
        if (user?.sub?.split("|")[1] === teacherUserDto.id) {
            setUserDto(teacherUserDto)
        }
        else if (user?.sub?.split("|")[1] === studentUserDto.id) {
            setUserDto(studentUserDto)
        }
        else if(user) {
            console.log(user)
            history.push("/new-user")
        }
    }, [user, userDto.name])

    return <UserContext.Provider value={{ userDto: userDto, setUserDto: setUserDto }}>
        {props.children}
    </UserContext.Provider>
}

export default UserContextProvider
