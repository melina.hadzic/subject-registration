import { createContext, useContext } from "react";
import {
  PassedSubject,
  RegisteredSubject,
  Student,
  Subject,
  Teacher,
  Request,
} from "../../api/dto-objects";

export interface ListValuesContextValue {
  subjects: Subject[];
  teachers: Teacher[];
  registeredSubjects: RegisteredSubject[];
  passedSubjects: PassedSubject[];
  students: Student[];
  requests: Request[];
}

const defaultValue: ListValuesContextValue = {
  subjects: [],
  teachers: [],
  registeredSubjects: [],
  passedSubjects: [],
  students: [],
  requests: [],
};

export const ListValuesContext = createContext<ListValuesContextValue>(
  defaultValue
);
export const ListValuesContextConsumer = ListValuesContext.Consumer;
export const useListValuesContext = (): ListValuesContextValue =>
  useContext(ListValuesContext);
