import React, { PropsWithChildren, useEffect, useState } from "react"
import agent from "../../api/agent"
import { PassedSubject, RegisteredSubject } from "../../api/dto-objects"
import { Role, useUserContext } from "../user-context/UserContext"
import { ListValuesContext, ListValuesContextValue } from "./ListValuesContext"

const ListValuesContextProvider: React.FC<PropsWithChildren<{}>> = (props: PropsWithChildren<{}>) => {
    const { userDto } = useUserContext()
    const [listValues, setListValues] = useState<ListValuesContextValue>({
        subjects: [],
        teachers: [],
        registeredSubjects: [],
        passedSubjects: [],
        students: [],
        requests: []
    })

    useEffect(() => {
        void (async () => {
            const subjects = await agent.Subjects.list()
            const teachers = await agent.Teachers.list()
            const students = await agent.Students.list()
            const requests = await agent.Requests.list()

            let registeredSubjects: RegisteredSubject[] = []
            let passedSubjects: PassedSubject[] = []

            if (userDto.role === Role.Student) {
                registeredSubjects = await agent.RegisteredSubjects.list(userDto.id)
                passedSubjects = await agent.PassedSubjects.list(userDto.id)
            }

            const _listValues: ListValuesContextValue = {
                subjects,
                teachers,
                registeredSubjects,
                passedSubjects,
                students,
                requests
            }
            setListValues(_listValues)
        })()
    }, [userDto])

    return <ListValuesContext.Provider value={listValues}>{props.children}</ListValuesContext.Provider>
}

export default ListValuesContextProvider
