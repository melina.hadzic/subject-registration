import React from "react";
import { useAuth0 } from '@auth0/auth0-react';
import { classes } from "../logout-button/LogoutButton.styles";

const LoginButton: React.FC = () => {
    const { loginWithRedirect, isAuthenticated } = useAuth0()

    return (
        <>
            {!isAuthenticated && (
                <div style={{ ...classes.logButtons }}>
                    <button style={{ ...classes.logButtons, cursor: "pointer" }} onClick={() => {
                        loginWithRedirect();
                    }}>
                        PRIJAVA
                    </button>
                </div>
            )
            }
        </>
    )
}

export default LoginButton
