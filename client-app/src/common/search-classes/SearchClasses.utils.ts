import { RegisteredSubject, Subject } from "../api/dto-objects";
import agent from "../api/agent";
import { UserDto } from "../contexts/user-context/UserContext";
import uuid from "react-uuid";

export const handleDeletedSubject = (subject: Subject) => {
  void (async () => {
    await agent.RegisteredSubjects.delete(subject.code);
  })();
};

export const registerSelectedClasses = (
  winterSubjects: Subject[],
  summerSubjects: Subject[],
  userDto: UserDto
): void => {
  winterSubjects.forEach((winterSubject) => {
    const registeredSubject: RegisteredSubject = {
      studentId: userDto.id,
      id: uuid(),
      code: winterSubject.code,
    };
    void (async () => {
      await agent.RegisteredSubjects.create(registeredSubject);
    })();
  });

  summerSubjects.forEach((summerSubject) => {
    const registeredSubject: RegisteredSubject = {
      studentId: userDto.id,
      id: uuid(),
      code: summerSubject.code,
    };
    void (async () => {
      await agent.RegisteredSubjects.create(registeredSubject);
    })();
  });
};

export const getNewSubject = () => {
  return {
    id: "",
    name: "",
    code: "",
    teacherId: "",
    prerequisites: "",
    isWinterSemesterSubject: false,
  };
};
