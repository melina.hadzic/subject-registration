import * as React from "react";
import Autocomplete from "@mui/material/Autocomplete";
import { useEffect, useState } from "react";
import { PermissionDialog } from "../permission-dialog/PermissionDialog";
import * as Mui from "@material-ui/core";
import { classes } from "./SearchClasses.styles";
import agent from "../api/agent";
import { Request, Subject } from "../api/dto-objects";
import LoadingComponent from "../../LoadingComponent";
import { useUserContext } from "../contexts/user-context/UserContext";
import { Button } from "semantic-ui-react";
import { useHistory } from "react-router-dom";
import { FilterOptionsValue } from "./SearchClasses.types";
import { getNewSubject, registerSelectedClasses } from "./SearchClasses.utils";
import { SubjectRow } from "./subject-row/SubjectRow";
import {FilterDropdown} from "./filter-dropdown/FilterDropdown";

export const SearchClasses: React.FC = () => {
    const { userDto } = useUserContext();
    const history = useHistory();

    const [subjects, setSubjects] = useState<Subject[]>([]);
    const [filteredSubjects, setFilteredSubjects] = useState<Subject[]>([]);
    const [winterSubjectsRequests, setWinterSubjectsRequests] = useState<Subject[]>([]);
    const [summerSubjectsRequests, setSummerSubjectsRequests] = useState<Subject[]>([]);
    const [winterSubjects, setWinterSubjects] = useState<Subject[]>([]);
    const [summerSubjects, setSummerSubjects] = useState<Subject[]>([]);
    const [currentUserPassedSubjects, setCurrentUserPassedSubjects] = useState<Subject[]>();

    const [isDialogOpen, setIsDialogOpen] = useState(false);
    const [loading, setLoading] = useState(true);
    const [secondLoading, setSecondLoading] = useState(false);
    const [currentClass, setCurrentClass] = useState<Subject>(getNewSubject());

    const [filter, setFilter] = useState<FilterOptionsValue>(
        FilterOptionsValue.NoFilter
    );

    const filterNoPrerequisitesUnfullfilledSubjects = (): void => {
        setPassedSubjects();
        filteredSubjects.forEach((subject) => {
            if (subject.prerequisites !== "") {
                const prerequisites = subject.prerequisites.split(",");
                const prerequisiteClasses = subjects.filter(
                    (s: Subject) =>
                        prerequisites.find((item: string) => item === s.code) !== undefined
                );
                prerequisiteClasses.forEach((eachClass) => {
                    if (
                        currentUserPassedSubjects?.find(
                            (i) => i.code === eachClass.code
                        ) === undefined
                    )
                        setFilteredSubjects((prevState) =>
                            prevState.filter((item) => item.code !== subject.code)
                        );
                });
            }
        });
    };

    const handleFilterSelected = (): void => {
        if (filter === FilterOptionsValue.NoFilter) {
            setFilteredSubjects(subjects);
            setFilteredSubjects((prevState) => {
                return prevState.filter(
                    (subject) =>
                        winterSubjects.find(
                            (winterSubject) => winterSubject.code === subject.code
                        ) === undefined
                );
            });
            setFilteredSubjects((prevState) => {
                return prevState.filter(
                    (subject) =>
                        summerSubjects.find(
                            (summerSubject) => summerSubject.code === subject.code
                        ) === undefined
                );
            });
        } else if (filter === FilterOptionsValue.WinterSubjects) {
            setFilteredSubjects(
                subjects.filter((subject: Subject) => subject.isWinterSemesterSubject)
            );
        } else if (filter === FilterOptionsValue.SummerSubjects) {
            setFilteredSubjects(
                subjects.filter((subject: Subject) => !subject.isWinterSemesterSubject)
            );
        } else {
            filterNoPrerequisitesUnfullfilledSubjects();
        }
        setPassedSubjects();
    }

    useEffect(() => {
        agent.Subjects.list().then((response) => {
            setSubjects(response);
            setFilteredSubjects(response);
        });
    }, []);

    const addRequestedSubjectToTheList = (classCode: string) => {
        if (currentClass.code === classCode) {
            if (currentClass.isWinterSemesterSubject)
                setWinterSubjectsRequests((prevState) => [...prevState, currentClass]);
            else setSummerSubjectsRequests((prevState) => [...prevState, currentClass]);
        }
    }

    const setPassedSubjects = () => {
        void (async () => {
            const allPassedSubjects = await agent.PassedSubjects.list(userDto.id);
            const currentUserPassedSubjects = subjects.filter((subject) =>
                allPassedSubjects.find((item) => item.code === subject.code)
            );
            setCurrentUserPassedSubjects(currentUserPassedSubjects);
            currentUserPassedSubjects.forEach((passed) => {
                setFilteredSubjects((prev) =>
                    prev.filter((item) => item.code !== passed.code)
                );
            });
        })();
    };

    useEffect(() => {
        setPassedSubjects();
        if (secondLoading) setSecondLoading(false);
    }, [userDto, subjects]);

    function handleInputChange(event: any, value: any) {
        const item = subjects.find((item: any) => item.name === value);
        if (item !== undefined) {
            if (item.isWinterSemesterSubject) {
                if ((winterSubjects.length + winterSubjectsRequests.length) === 5) {
                    alert("Za zimski semestar možete registrovati samo 5 predmeta.");
                    return;
                }
            } else {
                if ((summerSubjects.length + summerSubjectsRequests.length) === 5) {
                    alert("Za ljetni semestar možete registrovati samo 5 predmeta.");
                    return;
                }
            }

            setCurrentClass(item);
            const prerequisiteList: string[] = item.prerequisites.split(",");
            var cantAdd = false;
            if (item.prerequisites !== "") {
                prerequisiteList.forEach((prerequisite) => {
                    var item = currentUserPassedSubjects?.find(
                        (item) => item.code === prerequisite
                    );
                    if (item === undefined) {
                        cantAdd = true;
                        setIsDialogOpen(true);
                        return;
                    }
                });
            }

            if (!cantAdd) {
                if (item.isWinterSemesterSubject)
                    setWinterSubjects((prevState) => [...prevState, item]);
                else setSummerSubjects((prevState) => [...prevState, item]);
            }
        }
    }

    useEffect(() => {
        setFilteredSubjects((prevState) => {
            return prevState.filter(
                (subject) =>
                    winterSubjects.find(
                        (winterSubject) => winterSubject.code === subject.code
                    ) === undefined
            );
        });
        setFilteredSubjects((prevState) => {
            return prevState.filter(
                (subject) =>
                    winterSubjectsRequests.find(
                        (winterSubject) => winterSubject.code === subject.code
                    ) === undefined
            );
        });
        if (secondLoading) setSecondLoading(false);
    }, [winterSubjects, winterSubjectsRequests]);

    useEffect(() => {
        setFilteredSubjects((prevState) => {
            return prevState.filter(
                (subject) =>
                    summerSubjects.find(
                        (summerSubject) => summerSubject.code === subject.code
                    ) === undefined
            );
        });
        setFilteredSubjects((prevState) => {
            return prevState.filter(
                (subject) =>
                    summerSubjectsRequests.find(
                        (summerSubject) => summerSubject.code === subject.code
                    ) === undefined
            );
        });
        if (secondLoading) setSecondLoading(false);
    }, [summerSubjects, summerSubjectsRequests]);

    useEffect(() => {
        void (async () => {
            const allRegistered = await agent.RegisteredSubjects.list(userDto.id);
            const requests: Request[] = await agent.Requests.list();

            requests.filter(i => i.studentId === userDto.id).forEach((subject) => {
                const subjectWithAllDetails = subjects.find(
                    (item) => item.code === subject.subjectCode
                );
                if (subjectWithAllDetails?.isWinterSemesterSubject) {
                    setWinterSubjectsRequests((prev) => [...prev, subjectWithAllDetails]);
                } else if (subjectWithAllDetails?.isWinterSemesterSubject === false) {
                    setSummerSubjectsRequests((prev) => [...prev, subjectWithAllDetails]);
                }
            });


            if (allRegistered?.length) setSecondLoading(true);
            setFilteredSubjects((prevState) => {
                return prevState.filter(
                    (subject) =>
                        allRegistered.find(
                            (registeredSubject) => registeredSubject.code === subject.code
                        ) === undefined
                );
            });
            allRegistered.forEach((subject) => {
                const subjectWithAllDetails = subjects.find(
                    (item) => item.code === subject.code
                );
                if (subjectWithAllDetails?.isWinterSemesterSubject) {
                    setWinterSubjects((prev) => [...prev, subjectWithAllDetails]);
                } else if (subjectWithAllDetails?.isWinterSemesterSubject === false) {
                    setSummerSubjects((prev) => [...prev, subjectWithAllDetails]);
                }
            });
            setTimeout(() => {
                setLoading(false);
            }, 800);
        })();
    }, [userDto, subjects]);

    if (loading || secondLoading)
        return <LoadingComponent inverted={true} content={"Loading..."} />;

    return (
        <div style={{ marginTop: "50px" }}>
            <div
                style={{ ...classes.mainDiv, flexDirection: "column" }}
            >
                <Mui.Typography style={{ ...classes.mainTitle }}>
                    REGISTRACIJA PREDMETA
                </Mui.Typography>
                <div style={{ display: "flex", marginLeft: "250px", flexDirection: "row" }}>
                    <Mui.Typography style={{ ...classes.filterLabel }}>
                        FILTERI
                    </Mui.Typography>
                    <FilterDropdown filter={filter} handleFilterSelected={handleFilterSelected} setFilter={setFilter} />
                    <label style={{ ...classes.searchedSubjectLabel }}>
                        Traženi predmet:{" "}
                        <Autocomplete
                            key={currentClass.code}
                            onInputChange={handleInputChange}
                            sx={{
                                display: "inline-block",
                                "& input": {
                                    width: 400,
                                    padding: "10px",
                                    bgcolor: "background.paper",
                                    color: (theme) =>
                                        theme.palette.getContrastText(
                                            theme.palette.background.paper
                                        ),
                                },
                            }}
                            id="custom-input-demo"
                            options={filteredSubjects.map((subject: Subject) => subject.name)}
                            renderInput={(params) => (
                                <div ref={params.InputProps.ref}>
                                    <input
                                        placeholder="Naziv predmeta"
                                        type="text"
                                        style={{ fontSize: "20px" }}
                                        {...params.inputProps}
                                    />
                                </div>
                            )}
                        />
                    </label>
                </div>
            </div>
            {isDialogOpen && (
                <PermissionDialog
                    teacherId={currentClass.teacherId}
                    classId={currentClass.code}
                    onOverlayClick={() => {
                        setIsDialogOpen(false);
                    }}
                    prerequisites={currentClass?.prerequisites?.split(",") ?? []}
                    onRequestSent={() => {
                        addRequestedSubjectToTheList(currentClass.code)
                    }}
                />
            )}
            <div style={{ ...classes.flexWidth, flexDirection: "row" }}>
                <div style={{ ...classes.box, marginLeft: "60px" }}>
                    <Mui.Typography style={{ ...classes.selectedSubjectsTitle }}>
                        Izabrani predmeti u zimskom semestru
                    </Mui.Typography>
                    <Mui.Divider style={{ ...classes.divider }} />
                    {winterSubjects.length || winterSubjectsRequests.length ? (
                        winterSubjects.map((item) => {
                            return (
                                <SubjectRow isRequest={false} item={item} setFilteredSubjects={setFilteredSubjects} setSemesterSubjects={setWinterSubjects} />
                            );
                        })
                    ) : (
                        <div style={{ ...classes.noSelectedSubjects }}>
                            Nema izabranih predmeta.
                        </div>
                    )}
                    {winterSubjectsRequests.map((item) => {
                        return (
                            <SubjectRow item={item} setSemesterSubjects={setWinterSubjectsRequests} setFilteredSubjects={setFilteredSubjects} isRequest={true} />
                        );
                    })}
                </div>
                <div
                    style={{
                        ...classes.box,
                    }}
                >
                    <Mui.Typography style={{ ...classes.selectedSubjectsTitle }}>
                        Izabrani predmeti u ljetnom semestru
                    </Mui.Typography>
                    <Mui.Divider style={{ ...classes.divider }} />
                    {summerSubjects.length || summerSubjectsRequests.length ? (
                        summerSubjects.map((item) => {
                            return (
                                <SubjectRow isRequest={false} item={item} setFilteredSubjects={setFilteredSubjects} setSemesterSubjects={setSummerSubjects} />
                            );
                        })
                    ) : (
                        <div style={{ ...classes.noSelectedSubjects }}>
                            Nema izabranih predmeta.
                        </div>
                    )}
                    {summerSubjectsRequests.map((item) => {
                        return (
                            <SubjectRow item={item} setSemesterSubjects={setSummerSubjectsRequests} setFilteredSubjects={setFilteredSubjects} isRequest={true} />
                        );
                    })}
                </div>
            </div>
            <Button
                onClick={() => {
                    registerSelectedClasses(winterSubjects, summerSubjects, userDto)
                    history.push("/");
                }}
                style={{ ...classes.registrationButton }}
            >
                REGISTRACIJA SELEKTOVANIH PREDMETA
            </Button>
        </div>
    );
};
