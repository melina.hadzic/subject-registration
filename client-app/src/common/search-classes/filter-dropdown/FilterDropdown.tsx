import {classes} from "../SearchClasses.styles";
import * as Mui from "@material-ui/core";
import {filterOptions, FilterOptionsValue} from "../SearchClasses.types";
import * as React from "react";

export interface FilterDropdownProps {
    filter: FilterOptionsValue
    setFilter: (value: FilterOptionsValue) => void
    handleFilterSelected: any
}

export const FilterDropdown = (props: FilterDropdownProps): JSX.Element => {
    const { filter, setFilter, handleFilterSelected } = props
    return <Mui.Box sx={{ ...classes.muiBox }}>
        <Mui.FormControl fullWidth>
            <Mui.Select
                label="Filter"
                value={filter}
                onChange={(event) => {
                    setFilter(event.target.value as FilterOptionsValue);
                    handleFilterSelected();
                }}
            >
                {filterOptions.map((option) => (
                    <Mui.MenuItem value={option.value}>
                        {option.label}
                    </Mui.MenuItem>
                ))}
            </Mui.Select>
        </Mui.FormControl>
    </Mui.Box>
}
