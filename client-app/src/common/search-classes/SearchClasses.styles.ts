export const classes = {
  filterStyle: {
    display: "flex",
    marginLeft: "25px",
    margin: "10px",
  },
  filterCheckbox: {
    marginTop: "-8px",
    marginLeft: "5px",
  },
  divider: {
    backgroundColor: "rgb(247, 122, 38)",
    height: "2px",
  },
  selectedSubjectsTitle: {
    padding: "10px",
    fontSize: "20px",
  },
  subjectName: {
    padding: "10px",
    fontSize: "20px",
    margin: "5px",
  },
  box: {
    marginTop: "40px",
    border: "4px solid rgb(247, 122, 38)",
    width: "550px",
  },
  mainTitle: {
    letterSpacing: "2px",
    marginBottom: "20px",
    marginLeft: "500px",
    fontSize: "30px",
  },
  registrationButton: {
    marginLeft: "590px",
    marginTop: "20px",
    width: "350px",
    height: "55px",
    padding: "10px",
    color: "white",
    backgroundColor: "rgb(247, 122, 38)",
  },
  noSelectedSubjects: {
    padding: "10px",
    fontSize: "18px",
  },
  deleteIcon: {
    marginTop: "15px",
    marginRight: "20px",
  },
  mainDiv: {
    margin: "25px",
    fontSize: "25px",
    display: "flex",
    marginTop: "60px",
  },
  filterLabel: {
    fontSize: "20px",
    letterSpacing: "2px",
    marginTop: "10px",
  },
  muiBox: {
    width: 150,
    marginLeft: "20px",
    marginTop: "8px",
  },
  searchedSubjectLabel: {
    fontSize: "25px",
    marginLeft: "110px",
  },
  flexWidth: {
    display: "flex",
    marginLeft: "160px",
  },
  dividerMargin: {
    margin: "5px",
  },
};
