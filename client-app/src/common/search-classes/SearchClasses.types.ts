export enum FilterOptionsValue {
  NoFilter,
  WinterSubjects,
  SummerSubjects,
  NoMissingPrerequisites,
}

export const filterOptions = [
  {
    label: "Bez filtera",
    value: FilterOptionsValue.NoFilter,
  },
  {
    label: "Predmeti zimskog semestra",
    value: FilterOptionsValue.WinterSubjects,
  },
  {
    label: "Predmeti ljetnog semestra",
    value: FilterOptionsValue.SummerSubjects,
  },
  {
    label: "Predmeti za koje ispunjavate sve preduslove",
    value: FilterOptionsValue.NoMissingPrerequisites,
  },
];
