import {classes} from "../SearchClasses.styles";
import * as Mui from "@material-ui/core";
import DeleteIcon from "@mui/icons-material/Delete";
import {handleDeletedSubject} from "../SearchClasses.utils";
import Stack from "@mui/material/Stack";
import * as React from "react";
import {Subject} from "../../api/dto-objects";
import {SubjectProps} from "./SubjectRow.types";
import HourglassEmptyIcon from "@mui/icons-material/HourglassEmpty";

export const SubjectRow = (props: SubjectProps): JSX.Element => {
    const { item, setSemesterSubjects, setFilteredSubjects, isRequest } = props
    return <Stack direction="row" justifyContent="space-between">
        <div key={item.code} style={{ ...classes.subjectName }}>
            {item.name}
            <Mui.Divider style={{ ...classes.dividerMargin }} />
        </div>
        {isRequest ? (
            <HourglassEmptyIcon
                style={{ ...classes.deleteIcon }}
                fontSize="large"
            />
        ) : (<DeleteIcon
            style={{ ...classes.deleteIcon }}
            fontSize="large"
            onClick={() => {
                setSemesterSubjects((prevState: Subject[]) =>
                    prevState.filter(
                        (prevItem) => item.code !== prevItem.code
                    )
                );
                setFilteredSubjects((prevState: Subject[]) => [...prevState, item]);
                handleDeletedSubject(item);
            }}
        />
        )}
    </Stack>
}
