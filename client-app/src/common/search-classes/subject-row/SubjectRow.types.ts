import {Subject} from "../../api/dto-objects";

export interface SubjectProps {
    item: Subject
    setSemesterSubjects: any
    setFilteredSubjects: any
    isRequest: boolean
}
