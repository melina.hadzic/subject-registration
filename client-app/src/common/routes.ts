import { ExtractRouteParams, generatePath } from "react-router";

export interface RouteDefinition<TPath extends string> {
  path: TPath;
  buildPath: (props: ExtractRouteParams<TPath>) => string;
}

function createRouteDefinition<TPath extends string>(
  path: TPath
): RouteDefinition<TPath> {
  return {
    path,
    buildPath: (params) => generatePath(path, params),
  };
}

export const Routes = {
  homepage: createRouteDefinition("/"),
  subjects: createRouteDefinition("/subjects/passed"),
  registerSubjects: createRouteDefinition("/subjects/register"),
  requests: createRouteDefinition("/subjects/requests"),
  registeredSubjects: createRouteDefinition("/subjects/registered"),
  error: createRouteDefinition("/error-page"),
  newUser: createRouteDefinition("/new-user"),
  adminPage: createRouteDefinition("/admin-page")
};
