import React from 'react';
import { useAuth0 } from "@auth0/auth0-react";
import { guestUserDto, useUserContext } from '../contexts/user-context/UserContext';
import { classes } from './LogoutButton.styles';

const LogoutButton: React.FC = () => {
    const { logout, isAuthenticated } = useAuth0();
    const { setUserDto } = useUserContext()

    return (
        <>
            {isAuthenticated && (
                <div style={{ ...classes.logButtons, position: "fixed", top: "40px", right: "340px" }}>
                <button style={{ ...classes.logButtons, cursor: "pointer" }} onClick={() => {
                    setUserDto(guestUserDto)
                    logout()
                }}>
                    ODJAVA
                </button>
                </div>
            )}
        </>
    )
}

export default LogoutButton;
