export const classes = {
  logButtons: {
    width: "150px",
    color: "white",
    border: "none",
    backgroundColor: "rgb(287, 162, 78)",
    height: "30px",
    letterSpacing: 2,
  },
  mainTitle: {
    letterSpacing: "2px",
    marginBottom: "20px",
    marginLeft: "500px",
    fontSize: "30px",
  },
  loginButton: {
    color: "white",
    position: "static",
    top: "45px",
    right: "330px",
  },
};
