import {RegisteredSubject, Subject} from "../api/dto-objects";
import {Typography} from "@material-ui/core";
import {registeredSubjectsStyle} from "../../views/components/registered-subjects/RegisteredSubjects.styles";
import {requestsListStyles} from "../../views/components/requests-list/RequestsList.styles";
import * as React from "react";
import {useListValuesContext} from "../contexts/list-values-context/ListValuesContext";

export interface CardProps {
    registeredSubject: RegisteredSubject | Subject
}
export const SubjectCard = (props: CardProps): JSX.Element => {
    const { registeredSubject } = props
    const { teachers, subjects } = useListValuesContext()

    function getTeacherName(teacherId: string | undefined): string {
        return teachers.find(teacher => teacher.auth0Id === teacherId)?.name || ""
    }

    return <div key={registeredSubject.code} style={{ border: "1px solid orange", margin: "10px", padding: "10px" }}>
        <Typography style={{ ...registeredSubjectsStyle.subjectTitle }}> Naziv predmeta: {subjects?.find(subj => subj.code === registeredSubject.code)?.name}</Typography>
        <Typography style={{ ...requestsListStyles.fontStyle }}>Profesor: {getTeacherName(subjects?.find(subj => subj.code === registeredSubject.code)?.teacherId)}</Typography>
        <Typography style={{ ...requestsListStyles.fontStyle }}>{subjects?.find(subj => subj.code === registeredSubject.code)?.isWinterSemesterSubject ? "ZIMSKI SEMESTAR" : "LJETNI SEMESTAR"}</Typography>
    </div>
}
