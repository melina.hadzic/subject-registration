import { useEffect, useState } from "react"
import agent from "../api/agent"
import { Request, Subject } from "../api/dto-objects"
import "./PermissionDialog.css"
import uuid from "react-uuid"
import { classes } from "../logout-button/LogoutButton.styles"
import { useUserContext } from "../contexts/user-context/UserContext"
import { useListValuesContext } from "../contexts/list-values-context/ListValuesContext"
import LoadingComponent from "../../LoadingComponent";
import * as React from "react";

interface PermissionDialogProps {
    onOverlayClick: () => void,
    prerequisites: string[]
    teacherId: string
    classId: string
    onRequestSent: () => void

}
export const PermissionDialog: React.FC<PermissionDialogProps> = (props: PermissionDialogProps) => {
    const { onOverlayClick, prerequisites, teacherId, onRequestSent, classId } = props
    const { subjects } = useListValuesContext()
    const { userDto } = useUserContext()
    const [currentUserPassedSubjects, setCurrentUserPassedSubjects] = useState<Subject[]>()
    const [missingPrerequisites, setMissingPrerequisites] = useState<Subject[]>()
    const [loading, setLoading] = useState(true)

    const sendRequest = () => {
        const request: Request = {
            studentId: userDto.id,
            teacherId: teacherId,
            id: uuid(),
            subjectCode: classId,
            missingPrerequisites: missingPrerequisites?.map(item => item.code).join(",") ?? "",
            status: 0
        }

        agent.Requests.create(request)
        onRequestSent()
        onOverlayClick()
    }

    useEffect(() => {
        void (async () => {
            const allPassedSubjects = await agent.PassedSubjects.list(userDto.id)
            const passedSubjects = subjects.filter(subject => allPassedSubjects.find(item => item.code === subject.code))
            setCurrentUserPassedSubjects(passedSubjects)

            let temp: Subject[] = []
            prerequisites.forEach(item => {
                if (currentUserPassedSubjects?.find(passed => passed.code === item) === undefined) {
                    const subj = subjects?.find(subj => subj.code === item)
                    if (subj) temp.push(subj)
                }
            })
            setMissingPrerequisites(temp)
            setLoading(false)
        })()

    }, [userDto, currentUserPassedSubjects, props.prerequisites, subjects])
    if (loading || subjects == undefined) return <LoadingComponent inverted={true} content={'Loading...'} />

    return (
        <>
            <div className={"overlay"} onClick={props.onOverlayClick}>

            </div>
            <div className={"dialog-box"}>
                <p style={{ padding: "10px" }}>Za izabrani predmet ne ispunjavate sljedeće preduslove:</p>
                <ul>
                    {missingPrerequisites?.map(item => (
                        <li key={item.code}>{item.name}</li>
                    ))}
                </ul>
                <p style={{ padding: "10px" }}>Zatražite dozvolu predmetnog nastavnika ili izaberite drugi predmet.</p>
                <div className={"button-list"}>
                    <button style={{ ...classes.logButtons }} onClick={sendRequest}>Zatraži dozvolu</button>
                    <button style={{ ...classes.logButtons }} onClick={props.onOverlayClick}>Odustani</button>
                </div>
            </div>
        </>
    )
}
