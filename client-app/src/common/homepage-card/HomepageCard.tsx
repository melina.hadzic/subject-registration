import {
    Button,
    Card,
    CardActionArea,
    CardActions,
    CardContent,
    makeStyles,
    Typography,
} from "@material-ui/core";
import { useState } from "react";
import { HomepageCardProps } from "./HomepageCard.types";
import { useHistory } from "react-router-dom";
import { Role, useUserContext } from "../contexts/user-context/UserContext";
import { useAuth0 } from "@auth0/auth0-react";

const useStyles = makeStyles({
    root: {
        width: 310,
        margin: "20px",
        transition: "transform 0.15s ease-in-out",
    },
    cardHovered: {
        margin: "20px",
        transform: "scale3d(1.10, 1.10, 1.05)",
    },
    button: {
        width: "100px",
        height: "35px",
        color: "white",
        backgroundColor: "rgb(247, 122, 38)",
    },
    description: {
        height: "70px",
        fontSize: "15px",
    },
    title: {
        margin: "8px",
        fontSize: "18px",
    },
});

function HomepageCard(props: HomepageCardProps) {
    const { title, description, buttonTitle, path, img } = props;
    const { userDto } = useUserContext();
    const { loginWithRedirect } = useAuth0();
    const classes = useStyles();
    const history = useHistory();
    const [state, setState] = useState({
        raised: false,
        shadow: 1,
    });

    const handleClickOnCard = () => {
        if (userDto.role !== Role.Guest) {
            history.push(path);
        } else {
            loginWithRedirect();
        }
    };

    return (
        <Card
            className={classes.root}
            classes={{ root: state.raised ? classes.cardHovered : "" }}
            onMouseOver={() => setState({ raised: true, shadow: 1 })}
            onMouseOut={() => setState({ raised: false, shadow: 1 })}
            raised={state.raised}
            onClick={handleClickOnCard}
        >
            <img
                alt={"Missing icon"}
                height="140"
                src={
                    img === 0
                        ? require("./overview.png")
                        : img === 1
                            ? require("./read.png")
                            : img === 2
                                ? require("./write.png")
                                : require("./request.png")
                }
                title={props.title}
            />
            <CardActionArea>
                <Typography className={classes.title}>{title}</Typography>
                <CardContent>
                    <Typography
                        className={classes.description}
                        gutterBottom
                        variant="h5"
                        component="h2"
                    >
                        {description}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button
                    className={classes.button}
                    size="small"
                    color="primary"
                >
                    {buttonTitle}
                </Button>
            </CardActions>
        </Card>
    );
}

export default HomepageCard;
