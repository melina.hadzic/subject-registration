export interface HomepageCardProps {
  title: string;
  description: string;
  buttonTitle: string;
  path: string;
  img: number;
}
