export interface Subject {
  name: string;
  code: string;
  prerequisites: string;
  teacherId: string;
  id: string;
  isWinterSemesterSubject: boolean;
}

export interface Request {
  id: string;
  status: number;
  missingPrerequisites: string;
  teacherId: string;
  studentId: string;
  subjectCode: string;
}

export interface Teacher {
  id: string;
  auth0Id: string;
  name: string;
}

export interface Student {
  id: string;
  auth0Id: string;
  name: string;
  index: string;
  email: string;
}

export interface PassedSubject {
  id: string;
  studentId: string;
  date: Date;
  grade: number;
  code: string;
}

export interface RegisteredSubject {
  id: string;
  studentId: string;
  code: string;
}

export interface User {
  auth0id: string,
  studentIndex: string,
  name: string,
  email: string,
  role: number,
  isApproved: boolean
}
