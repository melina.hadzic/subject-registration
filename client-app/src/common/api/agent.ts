import axios, { AxiosResponse } from "axios";
import {
  Subject,
  Request,
  Teacher,
  Student,
  PassedSubject,
  RegisteredSubject, User,
} from "./dto-objects";
import {UserDto} from "../contexts/user-context/UserContext";

const sleep = (delay: number) => {
  return new Promise((resolve) => {
    setTimeout(resolve, delay);
  });
};

axios.defaults.baseURL = "http://localhost:5000/api";

axios.interceptors.response.use(async (response) => {
  try {
    await sleep(1000);
    return response;
  } catch (err) {
    console.log(err);
    return await Promise.reject(err);
  }
});

const responseBody = <T>(response: AxiosResponse<T>) => response.data;

const requests = {
  get: <T>(url: string) => axios.get<T>(url).then(responseBody),
};

const RegisteredSubjects = {
  list: (id: string) =>
    requests.get<RegisteredSubject[]>(`/registeredsubjects/${id}`),
  create: (subject: RegisteredSubject) =>
    axios.post<RegisteredSubject>("/registeredsubjects", subject),
  update: (subject: RegisteredSubject) =>
    axios.put<RegisteredSubject>(`/registeredsubjects/${subject.id}`, subject),
  delete: (id: string) =>
    axios.delete<RegisteredSubject>(`/registeredsubjects/${id}`),
};

const Subjects = {
  list: () => requests.get<Subject[]>("/subjects"),
};

const Teachers = {
  list: () => requests.get<Teacher[]>("/teachers"),
};

const Students = {
  list: () => requests.get<Student[]>("/students"),
};

const Requests = {
  list: () => requests.get<Request[]>("/requests"),
  create: (request: Request) => axios.post<Request>("/requests", request),
  update: (request: Request) =>
    axios.put<Request>(`/requests/${request.id}`, request),
  delete: (id: string) => axios.delete<Request>(`/requests/${id}`),
};

const PassedSubjects = {
  list: (id: string) => requests.get<PassedSubject[]>(`/passedsubjects/${id}`),
};

const Users = {
  list: () => requests.get<User[]>("/users"),
  create: (user: User) => axios.post<User>("/users", user),
  delete: (id: string) => axios.delete<User>(`/users/${id}`),
  update: (user: User) =>
      axios.put<User>(`/users`, user),
}

const agent = {
  Requests,
  Subjects,
  Teachers,
  Students,
  PassedSubjects,
  RegisteredSubjects,
  Users
};

export default agent;
