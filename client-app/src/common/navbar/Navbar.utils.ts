import { Role } from "../contexts/user-context/UserContext";

export interface Page {
  role: Role[];
  title: string;
  route: string;
}

export const navbarPages: Page[] = [
  {
    role: [Role.Teacher, Role.Student],
    title: "Pregled predmeta",
    route: "/subjects/passed",
  },
  {
    role: [Role.Student],
    title: "Registracija predmeta",
    route: "/subjects/register",
  },
  {
    role: [Role.Student],
    title: "Registrovani predmeti",
    route: "/subjects/registered",
  },
  {
    role: [Role.Student, Role.Teacher],
    title: "Trenutni zahtjevi",
    route: "/subjects/requests",
  },
];
