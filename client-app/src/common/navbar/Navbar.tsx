import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import { useHistory } from 'react-router-dom';
import LoginButton from "../login-button/LoginButton";
import LogoutButton from '../logout-button/LogoutButton';
import { Role, useUserContext } from '../contexts/user-context/UserContext';
import { useAuth0 } from '@auth0/auth0-react';
import { navbarPages, Page } from './Navbar.utils';

function Navbar() {
  const history = useHistory()
  const { userDto } = useUserContext()
  const { user } = useAuth0()

  return (
    <AppBar position="static" sx={{ backgroundColor: "rgb(247, 122, 38) !important" }}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <img src={require('./fet.png')} style={{ width: "100px", height: "100px", margin: "5px", marginRight: "20px" }} alt="fireSpot" />
          <Typography
            variant="h6"
            onClick={() => history.push("/")}
            sx={{
              mr: "20px !important",
              display: 'flex',
              fontFamily: 'monospace !important',
              fontWeight: "700 !important",
              letterSpacing: '.3rem !important',
              color: 'white !important',
              textDecoration: "none !important",
              cursor: "pointer"
            }}
          >
            POČETNA
          </Typography>
          <Box sx={{ flexGrow: 1, display: 'flex' }}>
            {navbarPages.filter((page: Page) => page.role.includes(userDto.role)).map((page) => (
              <Button
                key={page.title}
                onClick={() => history.push(page.route)}
                sx={{ my: 2, width: "200px", color: 'white !important', display: 'block' }}
              >
                {page.title}
              </Button>
            ))}
            <LoginButton />
            <LogoutButton />
          </Box>

          {userDto.role !== Role.Guest && <Box sx={{ flexGrow: "0 !important" }}>
            <Tooltip title={user?.name} >
              <>
                <IconButton sx={{ p: " 0 !important", marginBottom: "5px !important" }}>
                  <Avatar alt="Remy Sharp" src={user?.picture} />
                </IconButton>
                <Typography sx={{
                  color: "white !important",
                  marginTop: "2x !important",
                  marginLeft: "-18px !important",
                  fontWeight: "600 !important",
                  letterSpacing: "2px !important"
                }}>{userDto.role === Role.Student ? "STUDENT" : (
                  userDto.role === Role.Teacher ? "PROFESOR" : "Guest user"
                )}</Typography>
              </>
            </Tooltip>
          </Box>}
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default Navbar;
