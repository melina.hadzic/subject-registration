export const classes = {
  mainTitle: {
    letterSpacing: "2px",
    marginBottom: "20px",
    marginLeft: "500px",
    fontSize: "30px",
  },
  requestCard: {
    marginLeft: "5px",
    width: "670px",
    border: "1px solid rgb(247, 122, 38)",
    minHeight: "200px",
    margin: "15px",
  },
  subjectTitle: {
    fontSize: "18px",
    fontWeight: "600",
    padding: "10px",
    width: "650px",
    color: "white",
    backgroundColor: "rgb(287, 162, 78)",
  },
};
