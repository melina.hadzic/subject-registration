using Application.Students;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class StudentsController : BaseApiController {
        
        [HttpGet] 
        public async Task<ActionResult<List<Student>>> GetStudents() {
            return await Mediator.Send(new List.Query());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Student>> GetStudent(Guid id) {
            return await Mediator.Send(new Details.Query{Id = id});
        }
    }
}