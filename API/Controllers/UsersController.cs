using Application.Users;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class UsersController : BaseApiController {

        [HttpGet]
        public async Task<ActionResult<List<User>>> GetUsers() {
            return await Mediator.Send(new List.Query());
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser(User user) {
            return Ok(await Mediator.Send(new Create.Command{User = user}));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(string id) {
            return Ok(await Mediator.Send(new Delete.Command{Id = id}));
        }

        [HttpPut]
        public async Task<IActionResult> EditUser(User user) {
            return Ok(await Mediator.Send(new Edit.Command{User = user}));
        }
    }
}