using Application.PassedSubjects;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class PassedSubjectsController : BaseApiController
    {
        [HttpGet("{studentId}")] 
        public async Task<ActionResult<List<PassedSubject>>> GetPassedSubjects(string studentId) {
            return await Mediator.Send(new FilteredList.Query{StudentId = studentId});
        }
    }
}