using Application.Subjects;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class SubjectsController : BaseApiController {

        [HttpGet]
        public async Task<ActionResult<List<Subject>>> GetSubjects() {
            return await Mediator.Send(new List.Query());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Subject>> GetSubject(Guid id) {
            return await Mediator.Send(new Details.Query{Id = id});
        }
    }
}