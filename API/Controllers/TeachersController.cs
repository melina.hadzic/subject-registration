using Application.Teachers;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class TeachersController : BaseApiController {

        [HttpGet]
        public async Task<ActionResult<List<Teacher>>> GetTeachers() {
            return await Mediator.Send(new List.Query());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Teacher>> GetTeacher(Guid id) {
            return await Mediator.Send(new Details.Query{Id = id});
        }
    }
}