using Application.RegisteredSubjects;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class RegisteredSubjectsController : BaseApiController {

        [HttpGet("{studentId}")] 
        public async Task<ActionResult<List<RegisteredSubject>>> GetRegisteredSubjects(string studentId) {
            return await Mediator.Send(new List.Query{StudentId = studentId});
        }

        [HttpPost]
        public async Task<IActionResult> CreateRegisteredSubject(RegisteredSubject registeredSubject) {
            return Ok(await Mediator.Send(new Create.Command{RegisteredSubject = registeredSubject}));
        }

        [HttpDelete("{code}")]
        public async Task<IActionResult> DeleteRegisteredSubject(String code) {
            return Ok(await Mediator.Send(new Delete.Command{Code = code}));
        }
    }
}