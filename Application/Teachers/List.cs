using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Teachers
{
    public class List
    {
        public class Query : IRequest<List<Teacher>> {
        
        }

        public class Handler : IRequestHandler<Query, List<Teacher>>
        {
            private readonly DataContext _context;
            public Handler(DataContext context) {
                _context = context;
            }
            public async Task<List<Teacher>> Handle(Query request, CancellationToken cancellationToken)
            {
                return await _context.Teachers.ToListAsync();
            }
        }
    }
}