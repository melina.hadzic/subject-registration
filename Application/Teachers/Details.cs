using Domain;
using MediatR;
using Persistence;

namespace Application.Teachers
{
    public class Details
    {
        public class Query : IRequest<Teacher> {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, Teacher>
        {
            private readonly DataContext _context;

            public Handler(DataContext context) 
            {
                _context = context;
            }
            
            public async Task<Teacher> Handle(Query request, CancellationToken cancellationToken)
            {
                return await _context.Teachers.FindAsync(request.Id);
            }
        }
    } 
}