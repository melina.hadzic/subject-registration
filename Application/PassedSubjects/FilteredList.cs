using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.PassedSubjects
{
    public class FilteredList
    {
        public class Query : IRequest<List<PassedSubject>> {
            public string StudentId { get; set; }
        }

        public class Handler : IRequestHandler<Query, List<PassedSubject>>
        {
            private readonly DataContext _context;
            public Handler(DataContext context) {
                _context = context;
            }
            public async Task<List<PassedSubject>> Handle(Query request, CancellationToken cancellationToken)
            {
                return await _context.PassedSubjects.Where(x => x.StudentId == request.StudentId).ToListAsync();
            }
        }
    }
}