using MediatR;
using Persistence;

namespace Application.Requests
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context) 
            {
                _context = context;
            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var requestDomain = await _context.Requests.FindAsync(request.Id);

                // if(activity != null) {
                    _context.Remove(requestDomain);
                    await _context.SaveChangesAsync();
                // }

                return Unit.Value;
            }
        }
    }
}