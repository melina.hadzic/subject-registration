using MediatR;
using Domain;
using Persistence;
using AutoMapper;

namespace Application.Requests
{
    public class Edit
    {
        public class Command : IRequest
        {
            public Request RequestProp { get; set; }

        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var activity = await _context.Requests.FindAsync(request.RequestProp.Id);

                _mapper.Map(request.RequestProp, activity);

                await _context.SaveChangesAsync();

                return Unit.Value;
            }
        }
    }
}