using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Users
{
    public class Delete
    {
        public class Command : IRequest
        {
            public string Id { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context) 
            {
                _context = context;
            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var item = await _context.Users.Where(x => x.Auth0Id == request.Id).FirstOrDefaultAsync();

                 if(item != null) {
                    _context.Remove(item);
                    await _context.SaveChangesAsync();
                 }

                return Unit.Value;
            }
        }
    }
}