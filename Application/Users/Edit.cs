using MediatR;
using Domain;
using Persistence;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Application.Users
{
    public class Edit
    {
        public class Command : IRequest
        {
            public User User { get; set; }

        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.Where(x => x.Auth0Id == request.User.Auth0Id).FirstOrDefaultAsync();
                
                if(user != null) {
                    user.IsApproved = true;
                    await _context.SaveChangesAsync();
                }

                return Unit.Value;
            }
        }
    }
}