using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.RegisteredSubjects
{
    public class List
    {
        public class Query : IRequest<List<RegisteredSubject>> {
            public string StudentId { get; set; }
        }

        public class Handler : IRequestHandler<Query, List<RegisteredSubject>>
        {
            private readonly DataContext _context;
            public Handler(DataContext context) {
                _context = context;
            }
            public async Task<List<RegisteredSubject>> Handle(Query request, CancellationToken cancellationToken)
            {
                return await _context.RegisteredSubjects.Where(x => x.StudentId == request.StudentId).ToListAsync();
            }
        }
    }
}