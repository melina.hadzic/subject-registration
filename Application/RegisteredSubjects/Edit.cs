using MediatR;
using Domain;
using Persistence;
using AutoMapper;

namespace Application.RegisteredSubjects
{
    public class Edit
    {
        public class Command : IRequest
        {
            public RegisteredSubject RegisteredSubject { get; set; }

        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var activity = await _context.RegisteredSubjects.FindAsync(request.RegisteredSubject.Id);

                _mapper.Map(request.RegisteredSubject, activity);

                await _context.SaveChangesAsync();

                return Unit.Value;
            }
        }
    }
}