using MediatR;
using Domain;
using Persistence;

namespace Application.RegisteredSubjects
{
    public class Create
    {
        public class Command : IRequest
        {
            public RegisteredSubject RegisteredSubject { get; set; }
        }

        public class Handler : IRequestHandler<Command>
    {
        private readonly DataContext _context;
        public Handler(DataContext context)
        {
            _context = context;
        }
        public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
        {
            if(_context.RegisteredSubjects.Where(x => x.Code == request.RegisteredSubject.Code && x.StudentId == request.RegisteredSubject.StudentId).Any()) 
            return Unit.Value;

            _context.RegisteredSubjects.Add(request.RegisteredSubject);
            await _context.SaveChangesAsync();

            return Unit.Value;
        }
    }
    }
}