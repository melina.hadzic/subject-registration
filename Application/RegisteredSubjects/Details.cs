using Domain;
using MediatR;
using Persistence;

namespace Application.RegisteredSubjects
{
    public class Details
    {
        public class Query : IRequest<RegisteredSubject> {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, RegisteredSubject>
        {
            private readonly DataContext _context;

            public Handler(DataContext context) 
            {
                _context = context;
            }
            
            public async Task<RegisteredSubject> Handle(Query request, CancellationToken cancellationToken)
            {
                return await _context.RegisteredSubjects.FindAsync(request.Id);
            }
        }
    } 
}