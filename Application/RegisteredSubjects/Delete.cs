using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.RegisteredSubjects
{
    public class Delete
    {
        public class Command : IRequest
        {
            public String Code { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context) 
            {
                _context = context;
            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var activity = await _context.RegisteredSubjects.Where(x => x.Code == request.Code).Select(x => x).ToListAsync();

                 if(activity != null) {
                    _context.RemoveRange(activity);
                    await _context.SaveChangesAsync();
                 }

                return Unit.Value;
            }
        }
    }
}