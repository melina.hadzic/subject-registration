namespace Domain
{
    public class RegisteredSubject
    {
        public Guid Id { get; set; }
        public string StudentId { get; set; }
        public string Code { get; set; }
    }
}