namespace Domain
{
    public class Subject
    {
        public Guid Id { get; set; }
        public string Name { get; set; }  
        public string Code { get; set; }  
        public string TeacherId { get; set; }  
        public string Prerequisites { get; set; }
        public bool isWinterSemesterSubject { get; set; }
    }
}