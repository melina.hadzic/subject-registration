namespace Domain
{
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Auth0Id { get; set; }
        public string StudentIndex { get; set; }
        public int Role { get; set; }
        public bool IsApproved { get; set; }
    }
}