namespace Domain
{
    public class PassedSubject
    {
        public Guid Id { get; set; }
        public string StudentId { get; set; }
        public string Code { get; set; }
        public int Grade { get; set; }
        public DateTime Date { get; set; }
    }
}