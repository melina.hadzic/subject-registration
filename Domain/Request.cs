namespace Domain
{
    public class Request
    {
        public Guid Id { get; set; }
        public string StudentId { get; set; }
        public string TeacherId { get; set; }  
        public string SubjectCode { get; set; }
        public string MissingPrerequisites { get; set; }
        public int Status { get; set; }
    }
}