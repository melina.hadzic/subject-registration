namespace Domain
{
    public class Student
    {
        public Guid Id { get; set; }    
        public string Auth0Id { get; set; } 
        public string Name { get; set; }    
        public string Index { get; set; }
        public string Email { get; set; }  
    }
}