namespace Domain
{
    public class Teacher
    {
        public Guid Id { get; set; }    
        public string Auth0Id { get; set; }
        public string Name { get; set; }
        
    }
}