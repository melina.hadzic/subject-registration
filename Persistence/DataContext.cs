using Domain;
using Microsoft.EntityFrameworkCore;

namespace Persistence
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Request> Requests {get; set; }
        public DbSet<Teacher> Teachers {get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<PassedSubject> PassedSubjects { get; set; }
        public DbSet<RegisteredSubject> RegisteredSubjects { get; set; }
    }
}
