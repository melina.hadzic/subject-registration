using Domain;

namespace Persistence
{
    public class Seed
    {
        public static async Task SeedData(DataContext context)
        {
            if(!context.Users.Any()) {
                var users = new List<User>
                {
                    new User
                    {
                        Auth0Id = "111446754165250466163",
                        Name = "Melina Hadzic",
                        StudentIndex = "18136",
                        Email = "melina.hadzic@fet.ba",
                        Role = 0,
                        IsApproved = true
                    },
                    new User
                    {
                        Auth0Id = "63d813c37c9379f5545d0613",
                        Name = "Profesor",
                        StudentIndex = "",
                        Email = "melinahadzic@outlook.com",
                        Role = 1,
                        IsApproved = true
                    },
                };
                await context.Users.AddRangeAsync(users);
                await context.SaveChangesAsync();
            }

            if(!context.PassedSubjects.Any()) {
                var passedSubjects = new List<PassedSubject>
                {
                    new PassedSubject
                    {
                        Date = new DateTime(2021, 5, 5),
                        Code = "MAT1",
                        StudentId = "111446754165250466163",
                        Grade = 7
                    },
                    new PassedSubject 
                    {
                        Code = "RI001",
                        Date = new DateTime(2021, 6, 15),
                        StudentId = "111446754165250466163",
                        Grade = 9
                    },
                    new PassedSubject
                    {
                        Code="ESKE001",
                        Date = new DateTime(2021, 4, 25),
                        StudentId = "111446754165250466163",
                        Grade = 10
                    },
                    new PassedSubject
                    {
                        Code="ESKE002",
                        Date = new DateTime(2021, 1, 25),
                        StudentId = "111446754165250466163",
                        Grade = 9
                    },
                    new PassedSubject
                    {
                        Code="RI101",
                        Date = new DateTime(2021, 7, 12),
                        StudentId = "111446754165250466163",
                        Grade = 8
                    },
                    new PassedSubject
                    {
                        Code="FIZ1",
                        Date = new DateTime(2021, 4, 26),
                        StudentId = "111446754165250466163",
                        Grade = 8
                    },
                    new PassedSubject
                    {
                        Code="FIZ2",
                        Date = new DateTime(2021, 8, 6),
                        StudentId = "111446754165250466163",
                        Grade = 8
                    },
                    new PassedSubject
                    {
                        Code="TK001",
                        Date = new DateTime(2021, 5, 16),
                        StudentId = "111446754165250466163",
                        Grade = 7
                    },

                };
                
                await context.PassedSubjects.AddRangeAsync(passedSubjects);
                await context.SaveChangesAsync();
            }

            if(!context.Teachers.Any()) {
                var teachers = new List<Teacher>
                {
                    new Teacher 
                    {
                        Auth0Id = "63d813c37c9379f5545d0613",
                        Name = "RI Profesor",
                    },
                    new Teacher
                    {
                        Auth0Id = "1-test-id",
                        Name = "PMF Profesor - Matematika"
                    },
                    new Teacher 
                    {
                        Auth0Id = "2-test-id",
                        Name = "BMI Profesor"
                    },
                    new Teacher 
                    {
                        Auth0Id = "3-test-id",
                        Name = "ESKE Profesor"
                    },
                    new Teacher 
                    {
                        Auth0Id = "4-test-id",
                        Name = "PMF Profesor - Fizika"
                    },
                    new Teacher 
                    {
                        Auth0Id = "5-test-id",
                        Name = "EEMS Profesor"
                    },
                    new Teacher 
                    {
                        Auth0Id = "6-test-id",
                        Name = "TK Profesor"
                    },
                    new Teacher 
                    {
                        Auth0Id = "7-test-id",
                        Name = "AR Profesor"
                    },
                    new Teacher 
                    {
                        Auth0Id = "8-test-id",
                        Name = "RI Profesor 2"
                    }
                };
                await context.Teachers.AddRangeAsync(teachers);
                await context.SaveChangesAsync();
            }

            if(!context.Students.Any()) {
                var students = new List<Student> 
                {
                    new Student
                    {
                        Auth0Id = "111446754165250466163",
                        Name = "Melina Hadžić",
                        Index = "18136",
                        Email = "melina.hadzic@fet.ba"
                    },
                    new Student
                    {
                        Auth0Id = "test-student-id",
                        Name = "Ajla Glumčević",
                        Index = "18135",
                        Email = "ajla.glumcevic@fet.ba"
                    }
                };

                await context.Students.AddRangeAsync(students);
                await context.SaveChangesAsync();
            }

            if(!context.Subjects.Any()) {
            var subjects = new List<Subject> 
            {
                new Subject 
                {
                    Name = "Matematika 1",
                    TeacherId = "1-test-id",
                    Prerequisites = "",
                    Code = "MAT1",
                    isWinterSemesterSubject = true,
                },
                new Subject 
                {
                    Name = "Matematika 2",
                    TeacherId = "1-test-id",
                    Prerequisites = "MAT1",
                    Code = "MAT2",
                    isWinterSemesterSubject = false,
                },
                new Subject
                {
                    Name = "Klinički inžinjering",
                    TeacherId = "2-test-id",
                    Prerequisites = "ESKE001,RI101",
                    Code = "BMI201",
                    isWinterSemesterSubject = true,
                },
                new Subject 
                {
                    Code = "ESKE001",
                    Name = "Osnovi elektrotehnike 1",
                    TeacherId = "3-test-id",
                    Prerequisites = "",
                    isWinterSemesterSubject = true,
                },
                new Subject
                {
                    Code = "RI101",
                    Name = "Osnovi programiranja",
                    TeacherId = "63d813c37c9379f5545d0613",
                    Prerequisites = "RI001",
                    isWinterSemesterSubject = false,
                },
                new Subject
                {
                    Code = "RI001",
                    Name = "Osnovi računarstva",
                    TeacherId = "63d813c37c9379f5545d0613",
                    Prerequisites = "",
                    isWinterSemesterSubject = true,
                },
                new Subject 
                {
                    Code = "RI202",
                    Name = "Objektno orijentirano programiranje",
                    TeacherId = "63d813c37c9379f5545d0613",
                    Prerequisites = "RI101",
                    isWinterSemesterSubject = true,
                },
                new Subject {
                    Name = "Fizika 1",
                    TeacherId = "4-test-id",
                    Prerequisites = "",
                    Code = "FIZ1",
                    isWinterSemesterSubject = true,
                },
                new Subject {
                    Name = "Uvod u energetske sisteme",
                    TeacherId = "5-test-id",
                    Prerequisites = "",
                    Code = "EEMS001",
                    isWinterSemesterSubject = true,
                },
                new Subject {
                    Name = "Fizika 2",
                    TeacherId = "4-test-id",
                    Prerequisites = "FIZ1",
                    Code = "FIZ2",
                    isWinterSemesterSubject = false,
                },
                new Subject {
                    Name = "Osnovi elektrotehnike 2",
                    TeacherId = "3-test-id",
                    Prerequisites = "",
                    Code = "ESKE002",
                    isWinterSemesterSubject = false,
                },
                new Subject {
                    Name = "Tehnologije za podršku tehničkom pisanju",
                    TeacherId = "6-test-id",
                    Prerequisites = "",
                    Code = "TK001",
                    isWinterSemesterSubject = false,
                },
                new Subject {
                    Name = "Matematika 3",
                    TeacherId = "1-test-id",
                    Prerequisites = "",
                    Code = "MAT3",
                    isWinterSemesterSubject = true,
                },
                new Subject {
                    Name = "Signali i sistemi",
                    TeacherId = "6-test-id",
                    Prerequisites = "MAT1",
                    Code = "TK101",
                    isWinterSemesterSubject = true,
                },
                new Subject {
                    Name = "Osnovi elektronike",
                    TeacherId = "3-test-id",
                    Prerequisites = "ESKE001",
                    Code = "TK102",
                    isWinterSemesterSubject = true,
                },
                new Subject {
                    Name = "Matrične metode u elektrotehnici",
                    TeacherId = "5-test-id",
                    Prerequisites = "",
                    Code = "EEMS004",
                    isWinterSemesterSubject = true,
                },
                new Subject {
                    Name = "Primjena inženjerskih softverskih paketa",
                    TeacherId = "63d813c37c9379f5545d0613",
                    Prerequisites = "RI101",
                    Code = "RI206",
                    isWinterSemesterSubject = false,
                },
                new Subject {
                    Name = "Arhitektura računara",
                    TeacherId = "63d813c37c9379f5545d0613",
                    Prerequisites = "RI101",
                    Code = "RI201",
                    isWinterSemesterSubject = false,
                },
                new Subject {
                    Name = "Strukture podataka",
                    TeacherId = "63d813c37c9379f5545d0613",
                    Prerequisites = "RI202",
                    Code = "RI301",
                    isWinterSemesterSubject = false,
                },
                new Subject {
                    Name = "Uvod u računarske algoritme",
                    TeacherId = "63d813c37c9379f5545d0613",
                    Prerequisites = "RI101",
                    Code = "RI203",
                    isWinterSemesterSubject = false,
                },
                new Subject {
                    Name = "Operativni sistemi",
                    TeacherId = "63d813c37c9379f5545d0613",
                    Prerequisites = "RI201,RI301",
                    Code = "RI401",
                    isWinterSemesterSubject = true,
                },
                new Subject {
                    Name = "Baze podataka",
                    TeacherId = "8-test-id",
                    Prerequisites = "RI101",
                    Code = "RI207",
                    isWinterSemesterSubject = true,
                },
                new Subject {
                    Name = "Linearni sistemi automatskog upravljanja I",
                    TeacherId = "7-test-id",
                    Prerequisites = "MAT1,MAT2,FIZ1,FIZ2",
                    Code = "AR103",
                    isWinterSemesterSubject = true,
                },
                new Subject {
                    Name = "Dizajn kompajlera",
                    TeacherId = "63d813c37c9379f5545d0613",
                    Prerequisites = "RI201,RI301",
                    Code = "RI402",
                    isWinterSemesterSubject = true,
                },
            };

            await context.Subjects.AddRangeAsync(subjects);
            await context.SaveChangesAsync();
            }
        }
    }
}